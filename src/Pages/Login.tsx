import React, { useState } from "react";
import { Grid, Tab, Tabs, Box, Card, CardContent } from "@mui/material";
import styled from "styled-components";
import { UserLogin, AdminLogin } from "../Components/Login/";

const StyledCard = styled(Card)`
  width: 250px;
  height: 300px;
`;
export default function Login() {
  const [activeTab, setActiveTab] = useState(0);

  return (
    <Grid
      data-testid={"login"}
      container
      direction={"column"}
      justifyContent={"center"}
      alignItems={"center"}
      style={{ minHeight: "100vh" }}
    >
      <Grid item>
        <StyledCard>
          <Box
            sx={{
              borderBottom: 1,
              borderColor: "divider",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Tabs
              value={activeTab}
              onChange={(event, value) => setActiveTab(value)}
            >
              <Tab label="Customer" />
              <Tab label="Admin" />
            </Tabs>
          </Box>
          <CardContent style={{ height: 300 }}>
            {activeTab === 0 ? <UserLogin /> : <AdminLogin />}
          </CardContent>
        </StyledCard>
      </Grid>
    </Grid>
  );
}
