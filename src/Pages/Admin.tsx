import React from "react";
import { FormControlLabel, FormGroup, Grid, Switch } from "@mui/material";
import {UsersTable,OrderProgress}from "../Components/Admin"
import { useStores } from "../Stores";
import { observer } from "mobx-react";
import styled from "styled-components";

const StyledTableContainer = styled.div`
padding-top: 10px;
`
function Admin(){
  const {adminStore} = useStores();
  const handleChange = (event:React.ChangeEvent<HTMLInputElement>)=> {
    adminStore.updateRegistrationStatus(event.target.checked);
  }
  return(
    <>
      <Grid container spacing={1}>
        <Grid item>
          <FormGroup>
            <FormControlLabel
              control={<Switch checked={adminStore.registration} onChange={handleChange} />}
              label="Registration status"
            />
          </FormGroup>
        </Grid>
       <Grid item>
         <OrderProgress/>
       </Grid>
      </Grid>
      <StyledTableContainer>
        <UsersTable />
      </StyledTableContainer>

    </>
  )
}
export default observer(Admin)