import React from "react";
import { Routes, useParams, Route } from "react-router-dom";
import { useStores } from "../Stores";
import { OrderChoices, OrderPreview } from "../Components/Order";
const NoData = () => {
  return <h4>there is no open orders for you, contact the admin</h4>;
};
export default function Order() {
  const { id } = useParams();
  const { adminStore } = useStores();
  const doesHaveActiveOrder = (id: string | undefined) => {
    if (adminStore.activeMeal && id) {
      if (adminStore.activeMeal.userId === id) return <OrderChoices />;
    }
    return <NoData />;
  };
  return (
    <Routes>
      <Route path={``} element={doesHaveActiveOrder(id)} />
      <Route path={`/preview`} element={<OrderPreview />} />
    </Routes>
  );
}
