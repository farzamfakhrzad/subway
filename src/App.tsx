import React from "react";
import "./App.css";
import Dashboard from "./Components/Dashboard/Dashboard";
import { observer } from "mobx-react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import Login from "./Pages/Login";
function App() {
  return (
    <div className="App">
      <Router basename={"/"}>
        <Routes>
          <Route path={"/"} element={<Login />} />
          <Route path={"/*"} element={<Dashboard />} />
        </Routes>
      </Router>
    </div>
  );
}

export default observer(App);
