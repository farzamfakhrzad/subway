export interface mealDetails  {
  breadType: string;
  breadSize: 15 | 30;
  ovenBaked: boolean;
  tasteOfSandwich: string;
  extras: {
    extraBacon: boolean;
    doubleMeat: boolean;
    extraCheese: boolean;
  };
  vegetables: string[];
  sauces: string[];
};
export interface meal  {
  id: string;
  userId: string;
  date: string;
  open: boolean;
  link: string;
  activeStep?:number,
  rating:number
  orderDetail?: mealDetails;
};
export interface activeMeal extends  meal{
  activeStep:number
}
export interface userType  {
  firstName: string;
  lastName: string;
  date: string;
  id: string;
  isAdmin: boolean;
  canOrder:boolean;
  meal?: meal;
  previousMealIds?: string[];
};
