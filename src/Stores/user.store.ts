import RootStore from "./index";
import { makeAutoObservable } from "mobx";
import { mealDetails } from "./types";

class UserStore {
  rootStore: RootStore;
  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
  }
  submitRating(rate: number, id: string) {
    console.log(rate, id);
    this.rootStore.adminStore.meals = this.rootStore.adminStore.meals.map(
      (meal) => (meal.id === id ? { ...meal, rating: rate } : meal)
    );
  }
  submitMealChoices(data: mealDetails, id: string) {
    if (this.rootStore.adminStore.activeMeal) {
      this.rootStore.adminStore.activeMeal.orderDetail = data;
      this.rootStore.adminStore.activeMeal.activeStep = 1;
      const oldMeals = [...this.rootStore.adminStore.meals];
      oldMeals.push(this.rootStore.adminStore.activeMeal);
      this.rootStore.adminStore.meals = oldMeals;
      this.rootStore.adminStore.users = this.rootStore.adminStore.users.map(
        (user) =>
          user.id === id
            ? { ...user, meal: { ...user.meal, orderDetail: data } }
            : user
      );
    }
  }
}
export default UserStore;
