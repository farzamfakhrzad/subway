import RootStore from "./index";
import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

class UserStore {
  adminAuthed = false;
  loggedInUser = "";
  rootStore: RootStore;
  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
    //For admin stay logged in on refreshes
    makePersistable(this, {
      name: "authStore",
      properties: ["adminAuthed"],
      storage: window.sessionStorage,
    });
  }
  logout(){
    this.adminAuthed = false;
  }
  login(payload: {
    username?: string;
    password: string;
    isAdmin: boolean;
  }) {
    if (payload.isAdmin) {
      const adminCred = {
        username: process.env.REACT_APP_ADMIN_USER,
        password: process.env.REACT_APP_ADMIN_PASS,
      };
      const { username, password } = payload;
      if (
        JSON.stringify(adminCred) === JSON.stringify({ username, password })
      ) {
        this.adminAuthed = true;
        if (
          !(this.rootStore.adminStore.users.filter((e) => e.id === "constantAdminId").length > 0)
        ) {
          this.rootStore.adminStore.users.push({
            date: new Date().getTime().toString(),
            id: "constantAdminId",
            firstName: process.env.REACT_APP_ADMIN_FNAME ?? "test",
            lastName: process.env.REACT_APP_ADMIN_LNAME ?? "test",
            isAdmin: true,
            canOrder: false,
          });
        }
      }
    }else{
      if (this.rootStore.adminStore.users.some(user => user.id === payload.password)) {
        this.loggedInUser = payload.password;
      }
    }

  }

}
export default UserStore;
