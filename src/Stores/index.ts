import React from "react";
import AdminStore from "./admin.store";
import UserStore from "./user.store";
import AuthStore from "./auth.store";
class RootStore {
  adminStore: AdminStore;
  userStore: UserStore;
  authStore: AuthStore;
  constructor() {
    this.adminStore = new AdminStore(this);
    this.userStore = new UserStore(this);
    this.authStore = new AuthStore(this);
  }
}
const StoresContext = React.createContext(new RootStore());

export const useStores = () => React.useContext(StoresContext);
export default RootStore;
