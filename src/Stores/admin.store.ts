import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";
import { activeMeal, meal, mealDetails, userType } from "./types";
import RootStore from "./index";
import { nanoid } from "nanoid";
import { createLoginLink } from "../Utils/helpers";

class adminStore {
  registration = false;
  users: (
    | {
        date: string;
        meal: { orderDetail: mealDetails };
        firstName: string;
        lastName: string;
        previousMealIds?: string[];
        canOrder: boolean;
        id: string;
        isAdmin: boolean;
      }
    | userType
  )[] = [];
  meals: meal[] = [];
  activeMeal: activeMeal | null = null;
  rootStore: RootStore;
  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
    makePersistable(this, {
      name: "adminStore",
      properties: ["users", "activeMeal", "registration","meals"],
      storage: window.localStorage,
    });
  }

  get getUsers() {
    return this.users.filter((user) => !user.isAdmin);
  }
  getMealById(id:string){
    return this.meals.find((meal) => meal.id === id);
  }
  getUserById(id: string) {
    return this.users.find((user) => user.id === id);
  }
  updateRegistrationStatus(value: boolean) {
    this.registration = value;
  }


  closeOrder(id: string) {
    this.users = this.users.map((value) => {
      if (value.id === id) {
        let currentPreviousOrders = value.previousMealIds ? value.previousMealIds : [];
        console.log(this.activeMeal?.orderDetail,currentPreviousOrders,"fuck")
        if(this.activeMeal?.orderDetail) currentPreviousOrders?.push(this.activeMeal?.id)
        this.activeMeal = null;
        return { ...value, canOrder: false,previousMealIds:currentPreviousOrders };
      }
      return value.id === id ? { ...value, open: false } : value;
    });
  }
  openMeal(id: string) {
    this.users = this.users.map((value) => {
      if (value.id === id) {
        const mealId = nanoid(6);
        const loginLink = createLoginLink(id);
        const mealDate = new Date().toLocaleString();
        this.activeMeal = {
          ...value,
          id: mealId,
          userId: id,
          link: loginLink,
          open: true,
          activeStep: 0,
          rating:0,
          date: mealDate,
        };
        return { ...value, canOrder: true };
      }
      return value.id === id ? { ...value, open: true } : value;
    });
  }
  addUser(userInfo: userType) {
    this.users.push({ ...userInfo, id: nanoid(5) });
  }


}

export default adminStore;
