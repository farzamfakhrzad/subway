export const breadOptions = [
  {
    key: "9Grain",
    label: "9-Grain Wheat",
  },
  {
    key: "multiGrain",
    label: "Multi-grain Flatbread",
  },
  {
    key: "italianCheese",
    label: "Italian Herb & Cheese",
  },
  {
    key: "italian",
    label: "Italian",
  },
  {
    key: "flatBread",
    label: "Flatbread",
  },
  {
    key: "garlic",
    label: "Roasted Garlic",
  },
];
 export const breadSizes = [
   {
     key: "15",
     label: "15cm",
   },
   {
     key: "30",
     label: "30cm",
   }
 ]
export const tasteOptions = [
  {
    key: "turkeyHam",
    label: "Turkey Breast & Black Forest Ham",
  },
  {
    key: "steakCheese",
    label: "Steak & Cheese",
  },
  {
    key: "carved-turkey",
    label: "Carved turkey",
  },
  {
    key: "meatBallMarinara",
    label: "Meatball Marinara ",
  },
  {
    key: "subwayClub",
    label: "Subway Club",
  },
  {
    key: "italianBMT",
    label: "Italian B.M.T",
  },
  {
    key: "chickenBaconRanch",
    label: "Chicken & Bacon Ranch Melt",
  },
  {
    key: "chicken-bacon",
    label: "Chicken & bacon ranch melt",
  },
  {
    key: "roastBeef",
    label: "Roast Beef",
  },
  {
    key: "veggieDelite",
    label: "Veggie Delite",
  },
  {
    key: "spicyItalian",
    label: "Spicy Italian",
  },
  {
    key: "turkeyBreast",
    label: "Turkey Breast",
  },
  {
    key: "blackForestHam",
    label: "Black Forest Ham",
  },
  {
    key: "buffaloChicken",
    label: "Buffalo Chicken",
  },
  {
    key: "sweetOnion",
    label: "Sweet Onion Teriyaki",
  },
  {
    key: "coldCut",
    label: "Cold Cut Combo",
  },
];

export const vegetablesOptions = [
  {
    key: "cucumber",
    label: "Cucumber",
  },
  {
    key: "greenPepper",
    label: "Green Pepper",
  },
  {
    key: "lettuce",
    label: "Lettuce",
  },
  {
    key: "redOnion",
    label: "Red Onion",
  },
  {
    key: "spinach",
    label: "Spinach",
  },
  {
    key: "tomatoes",
    label: "Tomatoes",
  },
];

export const sauceOptions = [
  {
    key: "chipotleSouthwest",
    label: "Chipotle Southwest",
  },
  {

    key: "lightMayo",
    label: "Light Mayo"
  },
  {

    key: "mayo",
    label: "Mayo"
  },
  {

    key: "ranch",
    label: "Ranch"
  },
  {

    key: "subwayVinaigrette",
    label: "SUBWAY® Vinaigrette"
  },
  {
    key: "sweetOnion",
    label: "Sweet Onion",
  },
  {
    key: "redWineVinegar",
    label: "Red Wine Vinegar",
  },
];

