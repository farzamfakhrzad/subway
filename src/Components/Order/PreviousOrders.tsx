import React, { useEffect, useState } from "react";
import { Card, CardContent, Grid, Rating, Typography } from "@mui/material";
import { useStores } from "../../Stores";
import { observer } from "mobx-react";
import { useParams } from "react-router-dom";
import { meal } from "../../Stores/types";
import { OrderInfo } from "../General";

function PreviousOrders() {
  const [previousOrder, setPreviousOrder] = useState<meal[] | []>([]);
  const { id } = useParams();
  const { adminStore, userStore } = useStores();
  useEffect(() => {
    if (id) {
      const currentUser = adminStore.getUserById(id);
      if (currentUser) {
        if (currentUser.previousMealIds) {
          const currentPreviousMeals: meal[] = [];
          const previousMealIds = currentUser.previousMealIds;
          for (let id of previousMealIds) {
            const mealIds = adminStore.getMealById(id);
            mealIds && currentPreviousMeals.push(mealIds);
          }
          setPreviousOrder(currentPreviousMeals);
        }
      }
    }
  }, []);
  const submitRating = (rate: number | null, mealId: string) => {
    if (rate) {
      userStore.submitRating(rate, mealId);
    }
  };
  return (
    <div data-testid={"previousOrder"}>
      <Typography variant={"h6"} textAlign={"left"}>
        Previous orders
      </Typography>
      <Grid container direction={"row"} spacing={1}>
        {previousOrder.map((meal) => (
          <Grid item xs={12} sm={6} md={4} key={meal.id}>
            <Card>
              <Typography>{`Order ID:${meal.id}`}</Typography>
              <Typography variant={"caption"}>{`Date:${meal.date}`}</Typography>
              <CardContent>
                <OrderInfo info={meal.orderDetail} direction={"column"} />
                <Rating
                  value={meal.rating === 0 ? null : meal.rating}
                  readOnly={Boolean(meal.rating)}
                  onChange={(event, newValue) => {
                    submitRating(newValue, meal.id);
                  }}
                />
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}
export default observer(PreviousOrders);
