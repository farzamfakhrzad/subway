import React, { useEffect, useState } from "react";
import { CustomStepper, OrderInfo } from "../General";
import { observer } from "mobx-react";
import { useStores } from "../../Stores";
import { PreviousOrders } from "./";
import { Grid, Typography, Card, CardContent } from "@mui/material";
import { useParams } from "react-router-dom";
import styled from "styled-components";

const StyledInfoContainer = styled.div`
  margin-top: 10px;
`;

const NoData = () => {
  return (
    <>
      <Grid
        container
        direction={"column"}
        justifyContent={"center"}
        alignContent={"center"}
        alignItems={"center"}
      >
        <Grid item>
          <Typography variant={"h5"}>You don't have an active order</Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant={"caption"}>
            but you can rate your previous Orders here.
          </Typography>
        </Grid>
      </Grid>
    </>
  );
};

function OrderPreview() {
  const [userInfo, setUserInfo] = useState<{
    firstName: string;
    lastName: string;
  }>();

  const { adminStore } = useStores();
  const { id } = useParams();
  useEffect(() => {
    if (id) {
      const compactInfo = adminStore.getUserById(id);
      if (compactInfo)
        setUserInfo({
          firstName: compactInfo.firstName,
          lastName: compactInfo.lastName,
        });
    }
  }, []);
  const steps = [
    { label: "Order Opened", key: 0 },
    { label: "Order in progress", key: 1 },
    { label: "Order done", key: 2 },
  ];
  return (
    <>
      <Grid container spacing={1}>
        <Grid item>
          {adminStore.activeMeal ? (
            <Card>
              <CardContent>
                <CustomStepper
                  steps={steps}
                  activeStep={adminStore.activeMeal.activeStep}
                />
                <StyledInfoContainer>
                  <Grid container spacing={1}>
                    <Grid item>First name:{userInfo && userInfo.firstName}</Grid>
                    <Grid item>Last name:{userInfo && userInfo.lastName}</Grid>
                  </Grid>
                  <OrderInfo
                    info={adminStore.activeMeal?.orderDetail}
                    direction={"row"}
                  />
                </StyledInfoContainer>
              </CardContent>
            </Card>
          ) : (
            <NoData />
          )}
        </Grid>
       <Grid item>
         <PreviousOrders />
       </Grid>
      </Grid>
    </>
  );
}
export default observer(OrderPreview);
