import React from "react";
import { act, render, screen } from "@testing-library/react";
import {  PreviousOrders } from "./index";
import {BrowserRouter as Router} from "react-router-dom";

test("Checks if Previous Orders is rendered", async () => {
  act(() => {
    render(
      <Router>
        <PreviousOrders/>
      </Router>
    );
  });
 expect(await  screen.getByTestId("previousOrder")).toBeTruthy()
});
