import React from "react";
import { act, fireEvent, render, screen } from "@testing-library/react";
import {  OrderChoices } from "./index";
import {BrowserRouter as Router} from "react-router-dom";

test("Checks if User Choices form is rendering correctly", async () => {
  act(() => {
    render(
      <Router>
        <OrderChoices/>
      </Router>
    );
  });
  const breadType = await  screen.getByTestId("bread") as HTMLInputElement
  const breadSize = await  screen.getByTestId("size") as HTMLSelectElement
  const sandwichTaste = await  screen.getByTestId("taste") as HTMLSelectElement
  const vegetables = await  screen.getByTestId("vegetables") as HTMLSelectElement
  const sauces = await  screen.getByTestId("sauces") as HTMLSelectElement
  const bacon = await  screen.getByTestId("bacon") as HTMLInputElement
  const meat = await  screen.getByTestId("meat") as HTMLInputElement
  const cheese = await  screen.getByTestId("cheese") as HTMLInputElement
  const oven = await  screen.getByTestId("oven") as HTMLInputElement
  const button = await screen.getByText("Submit Order");
  expect(button).toBeDisabled()
  expect(breadType).toBeTruthy()
  expect(breadSize).toBeTruthy()
  expect(sandwichTaste).toBeTruthy()
  expect(vegetables).toBeTruthy()
  expect(sauces).toBeTruthy()
  expect(bacon).toBeTruthy()
  expect(meat).toBeTruthy()
  expect(cheese).toBeTruthy()
  expect(oven).toBeTruthy()
});
