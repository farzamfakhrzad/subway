import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import {
  Grid,
  Select,
  MenuItem,
  Switch,
  FormControlLabel,
  FormGroup,
  Checkbox,
  Card,
  CardContent,
  CardActions,
  Button,
} from "@mui/material";
import {
  breadOptions,
  breadSizes,
  tasteOptions,
  vegetablesOptions,
  sauceOptions,
} from "../../Utils/constants";
import { useStores } from "../../Stores";
import { useParams } from "react-router-dom";
import { observer } from "mobx-react";

function OrderChoices() {
  // const [locationPermission, setLocationPermission] = useState(false);
  // const [userLocation, setUserLocation] = useState<{
  //   lat: number;
  //   lng: number;
  // }>();
  const [defaultValue, setDefaultValue] = useState({
    breadType: "",
    breadSize: "",
    ovenBaked: false,
    tasteOfSandwich: "",
    extraBacon: false,
    doubleMeat: false,
    extraCheese: false,
    vegetables: [],
    sauces: [],
  });
  const { adminStore, userStore } = useStores();
  const { id } = useParams();
  const {
    handleSubmit,
    reset,
    control,
    formState: { isValid },
  } = useForm({
    mode: "onChange",
    defaultValues: defaultValue,
  });
  useEffect(() => {
    if (id) {
      const currentUser = adminStore.getUserById(id);
      if (currentUser) {
        if (currentUser.previousMealIds) {
          const previousMealIds = currentUser.previousMealIds;
          const previousMeals = adminStore.getMealById(
            previousMealIds[previousMealIds.length - 1]
          );
          if (previousMeals) {
            let defaultValue = {
              ...previousMeals.orderDetail,
              extraCheese: previousMeals.orderDetail?.extras.extraCheese,
              extraBacon: previousMeals.orderDetail?.extras.extraBacon,
              doubleMeat: previousMeals.orderDetail?.extras.doubleMeat,
            };
            // @ts-ignore
            setDefaultValue(defaultValue);
            // @ts-ignore
            reset(defaultValue);
          }
        }
      }
    }
  }, [reset]);

  const submitOrder = (data: any) => {
    let newDetails = {
      ...data,
      extras: {
        extraBacon: data.extraBacon,
        extraCheese: data.extraCheese,
        doubleMeat: data.doubleMeat,
      },
    };
    delete newDetails.extraCheese;
    delete newDetails.extraBacon;
    delete newDetails.doubleMeat;

    if (id) userStore.submitMealChoices(newDetails, id);
  };
  const hasPreviousOrder = (id: string | undefined) => {
    if (!id) return false;
    const currentUser = adminStore.getUserById(id);
    if (!currentUser) return false;
    if (!currentUser.previousMealIds) return false;
    return true;
  };
  // const getUserLocation = () => {
  //   //Found out today the geolocation does not work in iran :(

  //   const location = window.navigator && window.navigator.geolocation;
  //
  //   if (location) {
  //     location.getCurrentPosition(
  //       (position) => {
  //         setUserLocation({
  //           lat: position.coords.latitude,
  //           lng: position.coords.longitude,
  //         });
  //       },
  //       (error) => {
  //         setUserLocation({ lat: 0, lng: 0 });
  //       }
  //     );
  //
  //   }
  // };
  return (
    <Card>
      <form onSubmit={handleSubmit(submitOrder)}>
        <CardContent>
          {hasPreviousOrder(id) && (
            <p style={{ textAlign: "left" }}>
              Your previous order was filled automatically
            </p>
          )}
          <Grid container spacing={1} alignItems={"center"}>
            <Grid item xs={12} sm={6} md={4} lg={2}>
              <Controller
                name={"breadType"}
                control={control}
                rules={{ required: true }}
                render={({ field: { onChange, value } }) => (
                  <Select
                    inputProps={{ "data-testid": "bread" }}
                    value={value}
                    label="Bread type"
                    onChange={onChange}
                    style={{ width: "100%" }}
                  >
                    {breadOptions.map((bread) => (
                      <MenuItem key={bread.key} value={bread.key}>
                        {bread.label}
                      </MenuItem>
                    ))}
                  </Select>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
              <Controller
                name={"breadSize"}
                rules={{ required: true }}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <Select
                    data-testid={"size"}
                    value={value}
                    label="Bread size"
                    onChange={onChange}
                    style={{ width: "100%" }}
                  >
                    {breadSizes.map((size) => (
                      <MenuItem key={size.key} value={size.key}>
                        {size.label}
                      </MenuItem>
                    ))}
                  </Select>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
              <Controller
                name={"tasteOfSandwich"}
                rules={{ required: true }}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <Select
                    data-testid={"taste"}
                    value={value}
                    label="Sandwich taste"
                    onChange={onChange}
                    style={{ width: "100%" }}
                  >
                    {tasteOptions.map((taste) => (
                      <MenuItem key={taste.key} value={taste.key}>
                        {taste.label}
                      </MenuItem>
                    ))}
                  </Select>
                )}
              />
            </Grid>

            <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
              <Controller
                name={"vegetables"}
                rules={{ required: true }}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <Select
                    value={value}
                    label="Vegetables"
                    data-testid={"vegetables"}
                    multiple
                    onChange={onChange}
                    style={{ width: "100%" }}
                  >
                    {vegetablesOptions.map((vegetables) => (
                      <MenuItem key={vegetables.key} value={vegetables.key}>
                        {vegetables.label}
                      </MenuItem>
                    ))}
                  </Select>
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
              <Controller
                name={"sauces"}
                rules={{ required: true }}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <Select
                    value={value}
                    data-testid={"sauces"}
                    label="Sauces"
                    multiple
                    onChange={onChange}
                    style={{ width: "100%" }}
                  >
                    {sauceOptions.map((sauce) => (
                      <MenuItem key={sauce.key} value={sauce.key}>
                        {sauce.label}
                      </MenuItem>
                    ))}
                  </Select>
                )}
              />
            </Grid>

            <Grid container item xs={12} sm={6} md={5} lg={5} xl={2}>
              <Grid item>
                <Controller
                  name={"extraBacon"}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormGroup>
                      <FormControlLabel
                        control={
                          <Checkbox data-testid={"bacon"} checked={value} onChange={onChange} />
                        }
                        label="Extra bacon"
                      />
                    </FormGroup>
                  )}
                />
              </Grid>
              <Grid item>
                <Controller
                  name={"doubleMeat"}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormGroup>
                      <FormControlLabel
                        control={
                          <Checkbox data-testid={"meat"} checked={value} onChange={onChange} />
                        }
                        label="Double meat"
                      />
                    </FormGroup>
                  )}
                />
              </Grid>
              <Grid item>
                <Controller
                  name={"extraCheese"}
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <FormGroup>
                      <FormControlLabel
                        control={
                          <Checkbox data-testid={"cheese"} checked={value} onChange={onChange} />
                        }
                        label="Extra cheese"
                      />
                    </FormGroup>
                  )}
                />
              </Grid>
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
              <Controller
                name={"ovenBaked"}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <FormGroup>
                    <FormControlLabel
                      control={<Switch data-testid={"oven"} checked={value} onChange={onChange} />}
                      label="Oven Baked"
                      labelPlacement={"end"}
                    />
                  </FormGroup>
                )}
              />
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Button disabled={!isValid} type={"submit"} variant={"contained"}>
            {adminStore.activeMeal?.orderDetail ? "Edit Order" : "Submit Order"}
          </Button>
          {/*<Button onClick={() => getUserLocation()}>Share Location</Button>*/}
        </CardActions>
      </form>
    </Card>
  );
}
export default observer(OrderChoices);
