import React from "react";
import { Route, Routes } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";
import { useStores } from "../../Stores";
import { observer } from "mobx-react";
import NavBar from "./NavBar";
import { Container } from "@mui/material";
import Order from "../../Pages/Order";
import Admin from "../../Pages/Admin";
import styled from "styled-components";

const StyledContainer = styled(Container)`
  padding-top: 25px;
`;

function Dashboard() {
  const { authStore } = useStores();

  return (
    <>
      <NavBar data-testid={"navbar"} />
      <StyledContainer data-testid={"container"}>
        <Routes>
          <Route path={"/order/:id/*"} element={<Order />} />
          <Route
            path={"/admin"}
            element={
              <PrivateRoute authed={authStore.adminAuthed}>
                <Admin />
              </PrivateRoute>
            }
          />
        </Routes>
      </StyledContainer>
    </>
  );
}
export default observer(Dashboard);
