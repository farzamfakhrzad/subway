import React from "react";
import { act, render, screen } from "@testing-library/react";
import { Dashboard } from "./index";
import {
  BrowserRouter as Router,
} from "react-router-dom";
test("Renders DashBoard Page", async () => {
  act(() => {
    render(<Router>
      <Dashboard/>
    </Router> );
  });
  expect(await screen.getByRole("banner")).toBeTruthy();
  expect(await screen.getByTestId("container")).toBeTruthy();
});