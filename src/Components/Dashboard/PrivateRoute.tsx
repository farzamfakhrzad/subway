import React from "react";
// @ts-ignore
import { Navigate, Route } from "react-router-dom";
type PrivateRouteType = {
  authed: boolean;
  children: React.ReactNode;
};
function PrivateRoute({ authed, children }: PrivateRouteType) {
  return <>{authed ? children : <Navigate to="/" />}</>;
}
export default PrivateRoute;
