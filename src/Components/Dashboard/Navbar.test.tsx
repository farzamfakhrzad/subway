import React from "react";
import { act, render, screen } from "@testing-library/react";
import { NavBar } from "./index";
test("Renders Navbar", async () => {
  act(() => {
    render(
      <NavBar/>
   );
  });
  expect(await screen.getByRole("button",{name:"Logout"})).toBeTruthy();
  expect(await screen.getByRole("img")).toBeTruthy();
});