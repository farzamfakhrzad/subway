import React from "react";
import { act, render, screen } from "@testing-library/react";
import {  PrivateRoute } from "./index";
import { BrowserRouter as Router } from "react-router-dom";
test("Checks Private Route Redirection", async () => {
  let isAuthed = true;
  act(() => {
    render(
      <Router>
        <PrivateRoute authed={isAuthed}>
          <div>userAuthed</div>
        </PrivateRoute>
      </Router>
    );
  });

  expect(await screen.getByText("userAuthed")).toBeTruthy();
});
