import React from "react";
import {
  AppBar,
  IconButton,
  Toolbar,
  Typography,
  Button,
  Grid,
} from "@mui/material";
import SubwayLogo from "../../Assets/subwayLogo.png";
import { useNavigate } from "react-router-dom";
import { useStores } from "../../Stores";
export default function NavBar() {

  const navigate = useNavigate();
  const { authStore } = useStores();

  const logout = () => {
    authStore.logout();
    navigate("/");
  };

  return (
    <AppBar position="sticky">
      <Toolbar>
        <Grid container justifyContent={"space-between"} alignItems={"center"}>
          <Grid item>
            <img src={SubwayLogo} style={{ height: 50, width: 150 }} />
          </Grid>
          <Grid item>
            {authStore.adminAuthed && (
              <Button color="inherit" onClick={logout}>
                Logout
              </Button>
            )}
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
}
