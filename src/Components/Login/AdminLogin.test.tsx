import React from "react";
import { act, fireEvent, render, screen } from "@testing-library/react";
import {  AdminLogin } from "./index";
import {BrowserRouter as Router} from "react-router-dom";

test("Checks if Admin Login is rendered", async () => {
  act(() => {
    render(
      <Router>
        <AdminLogin/>
      </Router>
    );
  });
  const userInput = await  screen.getByRole("textbox",{name:"username"}) as HTMLInputElement
  const passInput = await  screen.getByRole("textbox",{name:"password"}) as HTMLInputElement
  const button = await screen.getByRole("button");
  expect(button).toBeDisabled()
  expect(userInput).toBeTruthy();
  expect(passInput).toBeTruthy();
  act(() => {
    fireEvent.change(userInput, { target: { value: "admin" } });
  });
  expect(userInput.value).toBe("admin");
  act(() => {
    fireEvent.change(passInput, { target: { value: "pass" } });
  });
  expect(passInput.value).toBe("pass");
});
