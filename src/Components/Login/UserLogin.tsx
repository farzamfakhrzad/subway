import React, { useEffect, useState } from "react";
import { Button, CardActions, Grid, TextField } from "@mui/material";
import { useStores } from "../../Stores";
import { useNavigate } from "react-router-dom";
import { observer } from "mobx-react";
import { autorun } from "mobx";

function UserLogin() {
  const [loginCode, setLoginCode] = useState<string>("");
  const { authStore } = useStores();
  const navigate = useNavigate();
  useEffect(
    () =>
      autorun(() => {
        if (authStore.loggedInUser) {
          navigate(`/order/${authStore.loggedInUser}`, { replace: true });
        }
      }),
    []
  );

  const handleLogin = () => {
    if (loginCode) authStore.login({ password: loginCode, isAdmin: false });
  };
  return (
    <>
      <Grid container direction={"column"} spacing={2}>
        <Grid item container direction={"column"} spacing={1}>
          <Grid item>
            <label style={{ fontSize: 10 }}>
              Enter the code given to you by the admin
            </label>
          </Grid>
          <Grid item>
            <TextField
              variant="outlined"
              label={"Login code"}
              value={loginCode}
              onChange={(event) => setLoginCode(event.target.value)}
            />
          </Grid>
        </Grid>
      </Grid>
      <CardActions style={{ justifyContent: "center" }}>
        <Button disabled={!loginCode} onClick={handleLogin}>
          Login
        </Button>
      </CardActions>
    </>
  );
}
export default observer(UserLogin);
