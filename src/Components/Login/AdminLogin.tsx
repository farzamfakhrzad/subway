import React, { useEffect, useState } from "react";
import { Button, CardActions, Grid, TextField } from "@mui/material";
import { useStores } from "../../Stores";
import { useNavigate } from "react-router-dom";
import { autorun } from "mobx";
export default function AdminLogin() {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const { authStore } = useStores();
  const navigate = useNavigate();
  useEffect(
    () =>
      autorun(() => {
        if (authStore.adminAuthed) navigate("/admin",{replace:true})
      }),
    []
  )
  const handleLogin = () => {
    authStore.login({ username, password, isAdmin: true });
  };

  return (
    <div>
      <Grid container direction={"column"} alignItems={"space-between"}>
        <Grid item container direction={"column"} spacing={2}>
          <Grid item>
            <TextField
              variant="outlined"
              label={"username"}
              value={username}
              onChange={(event) => setUsername(event.target.value)}
            />
          </Grid>
          <Grid item>
            <TextField
              variant="outlined"
              label={"password"}
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
          </Grid>
        </Grid>
      </Grid>
      <CardActions style={{ justifyContent: "center" }}>
        <Button disabled={!username && !password} onClick={handleLogin}>
          Login
        </Button>
      </CardActions>
    </div>
  );
}
