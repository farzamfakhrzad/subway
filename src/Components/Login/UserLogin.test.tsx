import React from "react";
import { act, fireEvent, render, screen } from "@testing-library/react";
import {  UserLogin } from "./index";
import {BrowserRouter as Router} from "react-router-dom";

test("Checks if User Login is rendered", async () => {
  act(() => {
    render(
      <Router>
        <UserLogin/>
      </Router>
    );
  });
  const loginCode = await  screen.getByRole("textbox",{name:"Login code"}) as HTMLInputElement
  const button = await screen.getByRole("button");
  expect(button).toBeDisabled()
  expect(loginCode).toBeTruthy();
  act(() => {
    fireEvent.change(loginCode, { target: { value: "testCode" } });
  });
  expect(loginCode.value).toBe("testCode");
});
