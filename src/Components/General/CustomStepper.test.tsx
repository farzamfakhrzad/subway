import React from "react";
import { act, render, screen } from "@testing-library/react";
import {  CustomStepper } from "./index";
test("Renders Stepper", async () => {
  const steps = [
    { label: "Order Opened", key: 0 },
    { label: "Order in progress", key: 1 },
    { label: "Order done", key: 2 },
  ];  act(() => {
    render(
      <CustomStepper steps={steps} activeStep={0}/>
    );
  });

 for(let step of steps){
   expect(await screen.getByText(step.label)).toBeTruthy();
 }
});
