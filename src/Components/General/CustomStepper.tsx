import React from "react"
import { Step, StepLabel, Stepper } from "@mui/material";
type stepperPropTypes = {
  steps:{
    label:string;
    key:string | number
  }[],
  activeStep:number
}
export default function CustomStepper({steps,activeStep}:stepperPropTypes){
  return(
    <Stepper activeStep={activeStep}>
      {steps.map((step)=> <Step key={step.key}>
          <StepLabel>
            {step.label}
          </StepLabel>
      </Step>)}
    </Stepper>
  )
}