import React from "react";
import { mealDetails } from "../../Stores/types";
import { Grid, Typography } from "@mui/material";
import styled from "styled-components";
type orderDetailProps = {
  info: mealDetails | undefined;
  direction: "column" | "row";
};
type extrasType = {
  extraBacon: boolean;
  extraCheese: boolean;
  doubleMeat: boolean;
};

const extrasLabels = {
  extraBacon: "Extra Bacon",
  extraCheese: "Extra Cheese",
  doubleMeat: "Double Meat",
};
const detailLabels = {
  breadType: "Bread type",
  breadSize: "Bread size",
  tasteOfSandwich: "Sandwich taste",
  ovenBaked: "Oven baked",
  extras: "Extras",
  vegetables: "Vegetables",
  sauces: "Sauces",
};
const BoldText = styled(Typography)`
  font-weight: bold;
  font-size: 14px;
`;

const DetailRow = ({
  detailKey,
  value,
}: {
  detailKey: string;
  value: string | undefined;
}) => (
  <Grid container spacing={1} alignItems={"center"}>
    <Grid item>
      <BoldText>{detailLabels[detailKey as keyof mealDetails]}:</BoldText>
    </Grid>
    <Grid item>
      <Typography>{value}</Typography>
    </Grid>
  </Grid>
);
export default function OrderInfo({
  info,
  direction = "row",
}: orderDetailProps) {
  const getExtras = (extras: extrasType | undefined | null) => {
    if (!extras) return "None";
    let extrasArray: string[] = [];
    let key: keyof extrasType;
    for (key in extras) {
      if (extras[key]) extrasArray.push(extrasLabels[key]);
    }
    return extrasArray.join(",");
  };

  const getValue = (key: string, value: any) => {
    if (key === "extras") return getExtras(value);
    if (key === "sauces" || key === "vegetables") return value.join(",");
    if (key === "ovenBaked") return value ? "Yes" : "No";
    return value;
  };
  return (
    <Grid container direction={direction} spacing={1} data-testid={"info"}>
      {info
        ? Object.keys(info).map((key) => (
            <Grid item key={key}>
              <DetailRow
                detailKey={key}
                value={getValue(key, info[key as keyof mealDetails])}
              />
            </Grid>
          ))
        : Object.keys(detailLabels).map((key) => (
            <Grid item key={key}>
              <DetailRow detailKey={key} key={key} value={"unknown"} />
            </Grid>
          ))}
    </Grid>
  );
}
