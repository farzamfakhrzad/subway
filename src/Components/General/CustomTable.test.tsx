import React from "react";
import { act, render, screen } from "@testing-library/react";
import { CustomTable } from "./index";
test("Renders Table", async () => {
  const columns = [
    { label: "First name", key: "firstName", id: "name" },
    { label: "Last name", key: "lastName", id: "lastname" },
    { label: "Login code", key: "id", id: "code" },
    { label: "Date", key: "date", id: "date" },
  ];
  const mockData = [
    {
      firstName: "f1",
      lastName: "l1",
      date: new Date().toLocaleDateString(),
      id: "hfdbjvd",
    },
    {
      firstName: "f2",
      lastName: "l2",
      date: new Date().toLocaleDateString(),
      id: "kjnkjnfv",
    },
    {
      firstName: "f3",
      lastName: "l3",
      date: new Date().toLocaleDateString(),
      id: "hfdkclsdjcbjvd",
    },
  ];

  act(() => {
    render(<CustomTable columns={columns} data={mockData} />);
  });

  const table = await screen.getByRole("table");
  expect(table).toBeTruthy();
  const rows = await  screen.getAllByRole("row");
  //the +1 is the header row
  expect(rows).toHaveLength(mockData.length+1)
  for(let row of rows){
    expect(row).toBeTruthy()
  }
});
