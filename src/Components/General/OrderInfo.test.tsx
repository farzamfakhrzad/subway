import React from "react";
import { act, render, screen } from "@testing-library/react";
import {  OrderInfo } from "./index";
import { mealDetails } from "../../Stores/types";

test("Checks if Info is rendered", async () => {
  const mockInfo = {breadSize: 30 as 15 | 30,
    breadType: "italianCheese",
    extras: {extraBacon: false, extraCheese: true, doubleMeat: true},
    ovenBaked: true,
    sauces: ["lightMayo", "subwayVinaigrette", "redWineVinegar"],
    tasteOfSandwich: "italianBMT",
    vegetables: ["lettuce", "redOnion", "spinach"]}
  act(() => {
    render(
      <OrderInfo info={mockInfo} direction={"row"}/>
    );
  });

  expect(await screen.getByTestId("info")).toBeTruthy();
});
