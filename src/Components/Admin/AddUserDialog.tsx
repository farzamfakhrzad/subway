import React, { FormEvent } from "react";
import { Controller, useForm } from "react-hook-form";
import { Button, DialogActions, Grid, TextField } from "@mui/material";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import { useStores } from "../../Stores";

type DialogProps = {
  open: boolean;
  onClose: () => void;
};

//TODO use deafult value for mui system
export default function AddUserDialog({ open, onClose }: DialogProps) {
  const { adminStore } = useStores();
  const { handleSubmit, control,formState: { isValid } } = useForm({
    mode: "onChange",
    defaultValues: { firstName: "", lastName: "" },
  });
  const onSubmit = (data: any) => {
    adminStore.addUser({
      ...data,
      isAdmin: false,
      date: new Date().toLocaleString(),
      canOrder: false,
    });
    onClose();
  };
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Add User</DialogTitle>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogContent>
          <Grid container direction={"column"} spacing={2}>
            <Grid item>
              <Controller
                rules={{required:true}}
                name={"firstName"}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    onChange={onChange}
                    value={value}
                    label={"First name"}
                  />
                )}
              />
            </Grid>
            <Grid item>
              <Controller
                rules={{required:true}}
                name={"lastName"}
                control={control}
                render={({ field: { onChange, value } }) => (
                  <TextField
                    onChange={onChange}
                    value={value}
                    label={"Last name"}
                  />
                )}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button disabled={!isValid} variant="contained" type={"submit"}>
            Add User
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}
