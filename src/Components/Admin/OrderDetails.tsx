import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import CustomStepper from "../General/CustomStepper";
import { useStores } from "../../Stores";
import { Button, Grid, Typography } from "@mui/material";
import OrderInfo from "../General/OrderInfo";
import styled from "styled-components";

const StyledInfoContainer = styled.div`
  margin-top: 10px;
`;

function OrderDetails() {
  const steps = [
    { label: "Order Opened", key: 0 },
    { label: "Order in progress", key: 1 },
    { label: "Order done", key: 2 },
  ];

  const [userInfo, setUserInfo] = useState<{
    firstName: string;
    lastName: string;
    date: string;
  } | null>(null);

  const { adminStore } = useStores();
  useEffect(() => {
    if (adminStore.activeMeal) getUserInfo(adminStore.activeMeal.userId);
  }, []);

  const getUserInfo = (id: string) => {
    const userData = adminStore.getUserById(id);
    if (userData)
      setUserInfo({
        firstName: userData.firstName,
        lastName: userData.lastName,
        date: userData.date,
      });
  };
  const getExternalLink = (link:string) => {
    return `${process.env.REACT_APP_Base_URL}${link}`;
  };
  return (
    <div>
      <CustomStepper
        data-testid="stepper"
        steps={steps}
        activeStep={
          adminStore.activeMeal ? adminStore.activeMeal.activeStep : 0
        }
      />
      <StyledInfoContainer data-testid="userInfo">
        <Typography style={{ textAlign: "left" }}>Customer info:</Typography>
        <Grid container spacing={1}>
          <Grid item>First name:{userInfo && userInfo.firstName}</Grid>
          <Grid item>Last name:{userInfo && userInfo.lastName}</Grid>
          <Grid item>
            Meal Date:{adminStore.activeMeal && adminStore.activeMeal.date}
          </Grid>
          <Grid item container alignItems={"center"} spacing={1}>
            <Grid item>
              <a
                target="_blank"
                rel="noreferrer"
                href={adminStore.activeMeal?.link}
              >
                Login Link
              </a>
            </Grid>
            <Grid item>
              <Button
                variant={"contained"}
                size={"small"}
                onClick={() => {
                  navigator.clipboard.writeText(
                    adminStore.activeMeal
                      ? getExternalLink(adminStore.activeMeal.link)
                      : ""
                  );
                }}
              >
                Copy link
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </StyledInfoContainer>
      <div>
        <OrderInfo
          info={adminStore.activeMeal?.orderDetail}
          direction={"row"}
        />
      </div>
    </div>
  );
}
export default observer(OrderDetails);
