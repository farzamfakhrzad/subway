import React from "react";
import { act, render, screen, fireEvent } from "@testing-library/react";
import AddUserDialog from "./AddUserDialog";

test("Renders UserDialog", async () => {
  act(() => {
    render(<AddUserDialog open={true} onClose={() => {}} />);
  });
  expect(await screen.getByRole("dialog", { name: "Add User" })).toBeTruthy();
  expect(
    await screen.getByRole("textbox", { name: "First name" })
  ).toBeTruthy();
  expect(await screen.getByRole("textbox", { name: "Last name" })).toBeTruthy();
  expect(await screen.getByRole("button")).toBeTruthy();
  const button = screen.getByRole("button");
  expect(button).toBeDisabled();
  const fNameInput = screen.getByRole("textbox", {
    name: "First name",
  }) as HTMLInputElement;
  act(() => {
    fireEvent.change(fNameInput, { target: { value: "fName" } });
  });
  const lNameInput = screen.getByRole("textbox", {
    name: "Last name",
  }) as HTMLInputElement;
  expect(fNameInput.value).toBe("fName");
  act(() => {
    fireEvent.change(lNameInput, { target: { value: "lName" } });
  });
  expect(lNameInput.value).toBe("lName");
});
