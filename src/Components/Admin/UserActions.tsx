import React, { useState } from "react";
import { Switch, FormGroup, FormControlLabel, Tooltip } from "@mui/material";
import { useStores } from "../../Stores";
import { observer } from "mobx-react";

function UserActions({ id }: { id: string }) {
  const { adminStore } = useStores();

  const isOrderActive = (id: string) => {
    if (!adminStore.activeMeal) return false;
    return adminStore.activeMeal.userId === id;
  };
  const canOrder = () => {
    if (!adminStore.registration) return false;
    if (!adminStore.activeMeal) return true;
  };
  const computeToolTipText = (id: string) => {
    if (!adminStore.registration) return "Registration is not currently open";
    if (!adminStore.activeMeal) return "Order status";
    if (adminStore.activeMeal.userId !== id)
      return "There can only be one open order";
    return "";
  };
  const [isOrderOpen, setIsOrderOpen] = useState(isOrderActive(id));
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setIsOrderOpen(event.target.checked);
    if (event.target.checked) {
      adminStore.openMeal(id);
    } else {
      adminStore.closeOrder(id);
    }
  };
  return (
    <FormGroup data-testid={"action"}>
      <Tooltip title={computeToolTipText(id)}>
        <FormControlLabel
          disabled={!isOrderActive(id) && !canOrder()}
          control={<Switch checked={isOrderOpen} onChange={handleChange} />}
          label="Order open"
        />
      </Tooltip>
    </FormGroup>
  );
}
export default observer(UserActions);
