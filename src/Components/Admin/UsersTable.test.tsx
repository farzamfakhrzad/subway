import React from "react";
import { act, render, screen } from "@testing-library/react";
import { UsersTable } from "./index";
test("Renders User Table", async () => {
  act(() => {
    render(<UsersTable/>);
  });
  expect(await screen.getByTestId("userTable")).toBeTruthy();
});