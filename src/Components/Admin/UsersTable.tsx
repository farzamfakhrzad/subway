import React, { useState } from "react";
import CustomTable from "../General/CustomTable";
import { observer } from "mobx-react";
import { useStores } from "../../Stores";
import { Button, Grid } from "@mui/material";
import { AddUserDialog, UserActions } from "./";

function UsersTable() {
  const [openUserDialog, setOpenUserDialog] = useState(false);
  const { adminStore } = useStores();
  const columns = [
    { label: "First name", key: "firstName", id: "name" },
    { label: "Last name", key: "lastName", id: "lastname" },
    { label: "Login code", key: "id", id: "code" },
    { label: "Date", key: "date", id: "date" },
  ];

  return (
    <div data-testid={"userTable"}>
      <Grid container justifyContent={"space-between"} alignItems={"center"}>
        <Grid item>Users</Grid>
        <Grid item>
          <Button variant="contained" onClick={() => setOpenUserDialog(true)}>
            Add User
          </Button>
        </Grid>
      </Grid>
      <CustomTable
        data={adminStore.getUsers}
        columns={columns}
        Actions={UserActions}
      />
      <AddUserDialog
        open={openUserDialog}
        onClose={() => setOpenUserDialog(false)}
      />
    </div>
  );
}

export default observer(UsersTable);
