import React from "react";
import { Card, CardContent, Grid, Typography } from "@mui/material";
import { observer } from "mobx-react";
import { useStores } from "../../Stores";
import { OrderDetails } from "./";
const NoData = () => {
  return (
    <Grid
      container
      direction={"column"}
      alignItems={"center"}
      justifyContent={"center"}
    >
      <Grid item>
        <Typography variant={"h6"}>No active meal orders!</Typography>
      </Grid>
      <Grid item>
        <Typography variant={"caption"}>
          open a users order status to see its progress
        </Typography>
      </Grid>
    </Grid>
  );
};
function OrderProgress() {
  const { adminStore } = useStores();

  return (
    <Card>
      {adminStore.activeMeal ? (
        <CardContent data-testid="content">
          <OrderDetails />
        </CardContent>
      ) : (
        <NoData  data-testid="noData"/>
      )}
    </Card>
  );
}
export default observer(OrderProgress);
