import React from "react";
import { act, render, screen, fireEvent } from "@testing-library/react";
import { OrderDetails } from "./index";
test("Renders Order Detail", async () => {
  act(() => {
    render(<OrderDetails/>);
  });
  expect(await screen.getByTestId("userInfo")).toBeTruthy();

});
