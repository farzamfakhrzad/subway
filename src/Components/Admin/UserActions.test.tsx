import React from "react";
import { act, render, screen } from "@testing-library/react";
import { UserActions } from "./index";
test("Renders User Action", async () => {
  act(() => {
    render(<UserActions id={"test"}/>);
  });
  expect(await screen.getByTestId("action")).toBeTruthy();
});