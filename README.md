# React Subway

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
###Description:
This app is for managing orders at a local subway sandwich shop.\
The feature set include:
* An admin panel for opening meal registration and order management
* Adding users as customers
* viewing current order 
* Users can log in with a login link
* Users can also login via a unique code
* Users can track their current order
* Users can customize their sandwich ingredients
* Users can use previous order data as current order data
* Users can rate their meal
###Tech details
* React 18
* Typescript as primary language
* Mobx as state manager
* Mobx-persist-store for saving data
* CRA as bootstrap framework
* MUI as Component library
* Styled-components for Component styling
* RTL as Testing tool
* Nanoid for id generation
* Prettier for code styling


## Getting the app up & running

The process is fairly simple just:
### `npm install`: for installing depends
### `npm run start`: for running app on local machine
### `npm test`: for running tests

###Notes:
* env files contains user & pass for the admin login
* the sandwich ingredients were retrivied from Subways website
